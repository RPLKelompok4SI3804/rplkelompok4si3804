Aplikasi Penilaian Kinerja Pegawai

Aplikasi ini memiliki fitur :
   Login : 
   1. Login untuk Pegawai biasa
   2. Login untuk Pegawai yang memiliki bawahan
   3. Login untuk Administrator

   Halaman Awal :
   Halaman awal pegawai terdapat : 
         1. nama karyawan 
         2. isi tahun penugasan
         3. list penugasan per pegawai
         4. perencanaan tugas dalam satu tahun 
         5. feedback sesuai dengan penugasan 


Diagram dibuat menanjak ke akhir karena progressnya selalu menambah
Semua penugasan di rekap seperti sistem penilaian

![MockUp.png](https://bitbucket.org/repo/b8o5BG/images/2585218414-MockUp.png)
  

Login
Sprint 1

Buat Login :
	Login untuk Pegawai 
	Login untuk Pegawai yang memiliki bawahan 
	Login untuk Administrator

Buat Menu Halaman Utama :
	Halaman Utama untuk Pegawai
	Halaman Utama untuk Pegawai yang memiliki bawahan
	Halaman Utama untuk Administrator